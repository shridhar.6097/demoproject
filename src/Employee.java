public class Employee 
{
private int empno;
private String name;
private int basic,hra,da,gross,it,netsal;

private void CalculateSalary()
{
	da = (int) (basic * 0.73);
	hra = (int) (basic * 0.10);
	gross = basic+da+hra;
	it = (int) (gross * 0.30);
	netsal = gross - it;
}

public int getEmpno() {
	return empno;
}

public void setEmpno(int empno) {
	this.empno = empno;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getBasic() {
	return basic;
}

public void setBasic(int basic) {
	this.basic = basic;
	CalculateSalary();
}


public int getHra() {
	return hra;
}

public int getDa() {
	return da;
}
public int getGross() {
	return gross;
}

public int getIt() {
	return it;
}

public int getNetsal() {
	return netsal;
}
}

